

/* 
   Hardik rana
   
   Hackerrank-Cracking the coding interview
*/

using namespace std;
unordered_map<string,int>::iterator it;

bool ransom_note(vector<string> magazine, vector<string> ransom) {
    unordered_map<string, int> map;
    for(int i = 0; i < magazine.size(); i++) {
    	it = map.find(magazine[i]);
    	if(it == map.end()) {
    		map.insert(pair<string, int>(magazine[i], 1));
    	} else {
    		map[magazine[i]]++;
    	}
    }
    for(int i = 0; i < ransom.size(); i++) {
    	it = map.find(ransom[i]);
    	if(it == map.end()) {
    		return false;
    	} else if(it->second == 0) {
			return false;
    	}
    	map[ransom[i]]--;
    }
    return true;
}
