function main() {
    const PI = Math.PI;

    // Write your code here. Read input using 'readLine()' and print output using 'console.log()'.
    var r = readLine();

    // Print the area of the circle:
    var area = PI * r * r;
    console.log(area);

    // Print the perimeter of the circle:
    var peri = 2 * PI * r;
    console.log(peri);
}