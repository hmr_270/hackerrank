function factorial(n) {
    var temp = n;
    var product = 1;
    if (n == 1)
        return 1;
    else {
        while (temp != 1) {
            product = product * temp;
            temp--;
        }
        return product;
    }
}