/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */


#include <bits/stdc++.h>
using namespace std;

int main() {
    int val,i,j;
    long long int sum=0;
    vector<int> a;
    for(int i=0;i<5;i++){
        cin>>val;
        sum+=val;
        a.push_back(val);
    }
    sort(a.begin(), a.end());
    cout<<sum-a[4]<<" "<<sum-a[0];
    return 0;
}
