/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>
using namespace std;


int main() {
    long long int sum=0;
    int n;
    cin>>n;
    int a[n];
    for(int i=0;i<n;i++)
    {
     scanf("%d",&a[i]);
     sum+=a[i];
    }
    printf("%lld",sum);
    return 0;
}
