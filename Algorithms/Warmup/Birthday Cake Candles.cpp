/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */


#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int n;
    cin>>n;
    int c[n],h=0,t=0;
    for(int i=0; i<n;i++)
        cin>>c[i];
    sort(c, c+n,greater<int>());
    h=c[0];
    for(int i=0; i<n; i++){
        if(h==c[i])
            t++;
    }
    cout<<t;
    return 0;
}
