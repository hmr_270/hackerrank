/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>
using namespace std;


#define TEN 10
#define MOD 1000000007


long long preSum[200007];

int main()
{
	string str;

	cin>>str;

	long long sum=0,powOf10=1;

	long len=str.length();


	for(long i=0;i<len;i++)
	{
		preSum[i]=(preSum[i-1]+(str[i]-48)*(i+1))%MOD;
	}

	for(long i=0;i<len;i++)
	{
		sum=(sum+preSum[len-i-1]*powOf10)%MOD;

		powOf10=(powOf10*10)%MOD;
	}
	cout<<sum;
}
