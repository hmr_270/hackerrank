/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>
using namespace std;
int countPairsWithDiffK(int arr[], int n, int k);
int main()
{
    int n,k;
    cin>>n>>k;
    int a[n];
    for(int i=0;i<n;i++)
    {
        cin>>a[i];
    }
    printf("%d\n",countPairsWithDiffK(a,n,k));
}
int countPairsWithDiffK(int arr[], int n, int k)
{
    int count = 0;
    sort(arr, arr+n);  // Sort array elements

    int l = 0;
    int r = 0;
    while(r < n)
    {
         if(arr[r] - arr[l] == k)
        {
              count++;
              l++;
              r++;
        }
         else if(arr[r] - arr[l] > k)
              l++;
         else // arr[r] - arr[l] < sum
              r++;
    }
    return count;
}
