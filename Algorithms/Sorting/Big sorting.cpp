/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */



#include<bits/stdc++.h>
using namespace std;

void sortlargenumbers(string arr[],int n);

bool compare(string str1,string str2);

int main()
{
    int n;
    cin>>n;
    string arr[n];
    for(int i=0;i<n;i++)
    {
        cin>>arr[i];
    }
    sortlargenumbers(arr, n);

    for (int i=0; i<n; i++)
      cout << arr[i] << endl;

    return 0;
}

void sortlargenumbers(string arr[],int n)
{
    sort(arr,arr+n,compare);
}

bool compare(string str1,string str2)
{
    int n1=str1.length(),n2=str2.length();

    //if lengths are different
    if(n1<n2)
        return true;
    if(n1>n2)
        return false;

    //if lengths are same
    for(int i=0;i<n1;i++)
    {
        if(str1[i]<str2[i])
            return true;
        if(str1[i]>str2[i])
            return false;
    }
    return false;

}
