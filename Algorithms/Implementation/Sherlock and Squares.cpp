/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */
 
#include<bits/stdc++.h>
using namespace std;
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
      long int a,b;
      cin>>a>>b;
      int count = (int)(floor(sqrt(b))- ceil(sqrt(a)))+1;
      printf("%d\n",count);
    }
}
