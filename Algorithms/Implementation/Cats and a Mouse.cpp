/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>
using namespace std;
int main()
{
    int q;
    cin>>q;
    int x,y,z;
    while(q--)
    {
        cin>>x>>y>>z;
        if(abs(x-z)<abs(y-z))
            printf("Cat A\n");
        else if(abs(x-z)>abs(y-z))
            printf("Cat B\n");
        else
            printf("Mouse C\n");
    }
}

