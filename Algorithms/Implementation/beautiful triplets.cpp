/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int n,d,total=0;
    cin>>n>>d;
    int a[n];
    for(int i=0;i<n;i++) {
        cin>>a[i];
    }
    for(int i = 0; i < n; i++){
        for(int j = i+1; j < n; j++){
            if(a[j] - a[i] == d){
                for(int k = j+1; k < n; k++){
                    if(a[k] - a[j] == d){
                        total++;
                    }
                }
            }
        }
    }
    cout<<total;
    return 0;
}
