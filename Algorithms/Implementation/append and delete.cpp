/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>

using namespace std;

int main(){
    string s,t;
    cin>>s>>t;
    int k;
    cin>>k;
    int a= s.length();
    int b= t.length();
    int not_match=-1;
    a--;
    b--;
    for(int i=0;i<=min(a,b);i++)
    {
    if(s[i]!=t[i])
    {
       not_match=i;
       break;
    }
    }
    if(not_match!=-1)
    {
    if(a<=b)
    k=k-(2*(a-not_match+1)+(b-a));
    else
    k=k-(2*(b-not_match+1)+(a-b));
    }
    else   //not_match==-1
    k=k-abs(a-b);
    //cout<<"k="<<k<<endl<<"not_match="<<not_match<<endl;
    if(k<0)
    cout<<"No"<<endl;
    else if(k==0)
    cout<<"Yes"<<endl;
    else
    {
    if(not_match==0)
    cout<<"Yes"<<endl;
    else if(k%2==0)
    cout<<"Yes"<<endl;
    else
    {
    if(k>=2*b)
    cout<<"Yes"<<endl;
    else
    cout<<"No"<<endl;
    }
    }
    return 0;
}
