/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    int t;
    cin >> t;
    while(t--)
    {
    int n;
    int k;
    cin >> n >> k;
    int a[n];
    int CP=0;
    int CN=0;
    for(int i=0;i<n;i++)
    {
       cin>>a[i];
       if(a[i]<=0)
           CN++;
        else if(a[i]>0)
           CP++;
     }
    if(CN<k)
     printf("YES\n");
    else
     printf("NO\n");
    }
    return 0;
}
