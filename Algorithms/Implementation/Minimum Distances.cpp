/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>
using namespace std;


int minimum(int count[],int k);

int main()
{
    int n;
    cin>>n;
    int a[n];
    for(int i=0;i<n;i++)
    {
        cin>>a[i];
    }
    int count[n]={0},k=0;
    for(int i=0;i<n-1;i++)
    {
        for(int j=i+1;j<n;j++)
        {
            if(a[i]==a[j])
            {
                count[k]=abs(i-j);
                k++;
            }
        }
    }
    if(k==0)
        printf("-1\n");
    else
    {
        int min=minimum(count,k);
        printf("%d\n",min);
    }
}

int minimum(int count[],int k)
{
    int min1=count[0];
    for(int i=1;i<k;i++)
    {
        if(count[i]<min1)
            min1=count[i];
    }
    return min1;
}
