/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>
using namespace std;
int main()
{
    int b,w,x,y,z;
    int t;
    cin>>t;
    while(t--)
    {
        int price=0;
        cin>>b>>w;
        cin>>x>>y>>z;
        int r=b*x;
        int e=w*y;
        int q=b*z;
        int s=w*z;
        if(r==e)
        {
            price=r+e;
            printf("%d\n",price);
        }
        else if(r>e && x>=z)
        {
         price=b*(y+z)+e;
         printf("%d\n",price);
        }
        else if(r<e && y>=z)
        {
           price=r+w*(x+z);
           printf("%d\n",price);
        }
        else if(r>e && z>x)
        {
         price=r+e;
         printf("%d\n",price);
        }
        else if(r<e && z>y)
        {
           price=r+e;
           printf("%d\n",price);
        }
    }
    return 0;
}
