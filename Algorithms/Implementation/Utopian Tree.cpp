/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main(){
    int t;
    cin >> t;
    for(int a0 = 0; a0 < t; a0++){
        int n;
        cin >> n;
        int h=1;
        for(int i=1;i<=n;i++)
        {
           if(i%2!=0)
               h=h*2;
            else if(i%2==0)
                h=h+1;
        }
        printf("%d\n",h);
    }
    return 0;
}
