/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>
using namespace std;


int main() {
    string s;
    bool value;
    int t;
    cin>>t;
    while(t--)
    {
    cin>>s;
    value=next_permutation(s.begin(),s.end());
    if(value==false)
        printf("no answer\n");
    else
        cout<<s<<endl;
    }
}
