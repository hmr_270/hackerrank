/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include <bits/stdc++.h>
using namespace std;

int maxXORInRange(int L, int R)
{

    int LXR = L ^ R;

    //printf("%d\n",LXR);

    int msbPos = 0;
    while (LXR)
    {
        msbPos++;
        //printf("%d ",msbPos);
        LXR >>= 1;
        //printf("\n%d",LXR);
    }

    int maxXOR = 0;
    int two = 1;
    while (msbPos--)
    {
        maxXOR += two;
        two <<= 1;
    }

    return maxXOR;
}

int main()
{
    int l,r;
    cin>>l;
    cin>>r;
    cout << maxXORInRange(l,r) << endl;
    return 0;
}
