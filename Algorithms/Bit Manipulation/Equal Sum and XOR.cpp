
/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 algorithm:
 //
 Since we know a + b = a ^ b + a & b

  We can write, n + i = n ^ i + n & i

  So n + i = n ^ i implies n & i = 0

  Hence our problem reduces to finding values of i such that n & i = 0.
    We can use the count of unset-bits in the binary representation of n.
    For n & i to be zero, i must unset all set-bits of n.
    If the kth bit is set at a particular in n, kth bit in i must be 0 always, else kth bit of i can be 0 or 1

  Hence, total such combinations are 2^(count of unset bits in n)
  //
*/


#include <bits/stdc++.h>
using namespace std;


long long int countValues(int n)
{
    int unset_bits=0;
    while (n)
    {
        if ((n & 1) == 0)
            unset_bits++;
        n=n>>1;
    }
 
    // Return 2 ^ unset_bits
    return 1 << unset_bits;
}

// Driver code
int main()
{
    long long int n;
    cin>>n;
    cout << countValues(n);
    return 0;
}
