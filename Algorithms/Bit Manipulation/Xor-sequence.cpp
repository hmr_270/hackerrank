/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ALGORITHMS
 
 */

#include<bits/stdc++.h>
using namespace std;

long long int compute(long long int x);

int main()
{
  int t;
  cin>>t;
  while(t--)
  {
  long long int l,r;
  cin>>l>>r;
  long long int answer=compute(r)^compute(l-1);
  printf("%lld\n",answer);
  }
}
long long int compute(long long int x)
{
    long long int y=x%8;
    if(y==0 || y==1)
        return(x);
    else if(y==2 || y==2)
        return 2;
    else if(y==4 || y==5)
        return(x+2);
    else if(y==6||y==7)
        return 0;
  
   return 0;
}
