/*
  HARDIK RANA

  30 DAYS OF CODE-[Day 3]

*/

#include <bits/stdc++.h>
using namespace std;

int main()
{
    int n;
    cin >> n;
    if(n%2!=0)
        printf("Weird");
    else if(n%2==0&&n>=2&&n<=5)
        printf("Not Weird");
    else if(n%2==0&&n>=6&&n<=20)
        printf("Weird");
    else if(n%2==0&&n>=20)
        printf("Not Weird");
}
