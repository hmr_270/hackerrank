/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ProjectEuler
 
 */

#include<bits/stdc++.h>
using namespace std;
int main()
{
    long long int T,N;
    long long diff,a,b,i;
    scanf("%lld",&T);
    while(T--)
    {scanf("%lld",&N);
     a=pow(((N*(N+1))/2),2);
     b=((N*(N+1)*((2*N)+1))/6);
     diff=a-b;
     if(diff<0)
     diff=diff*(-1);
     printf("%lld\n",diff);}
     return 0;}

