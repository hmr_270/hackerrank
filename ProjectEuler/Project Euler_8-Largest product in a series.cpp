
/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ProjectEuler
 
 */

#include <iostream>
using namespace std;
int mul( std::string a , int i , int n )
{
    int mult = 1 ;
    for ( int j = i ; j < n ; j++ )
        mult *= a[j] - '0' ;
    return mult ;
}

int main()
{
    int t , n , k ;
    std::string number ;
    std::cin >> t ;
    int max_1 = 0 ;
    int temp ;
    while( t-- )
    {
        max_1 = 0 ;
        std::cin >> n >> k ;
        std::cin >> number ;
        for ( int i = 0 ; i < n - k ; i++ )
        {
            temp = mul(number,i,i+k);
            if (temp > max_1)
                max_1 = temp ;
        }
        std::cout<< max_1 << std::endl ;
    }
}
