/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ProjectEuler
 
 */

#include <bits/stdc++.h>

using namespace std ;

int main()
{
	int arr[200000] , num , test ;
	cin >> test ;
	int siz = sizeof(arr)/sizeof(arr[0]) ;
	fill_n(arr,siz,true) ;

    for(int p = 2 ; p <= sqrt(siz) ; p++)
    {
    	for(int i = 2*p ; i < siz ; i += p)
    		arr[i] = false ;
	}
	while(test--)
	{
    cin >> num ;
	int count = 0 ;
	for(int i = 2 ; i < siz ; i++)
	{
	 if(arr[i] == true) count++;
	 if(count == num)
	 {
	 cout << i << endl ;
	 break ;
     }
    }
 }
}
