/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:ProjectEuler
 
 */

#include <bits/stdc++.h>
using namespace std;

int main ()
{
    int nCases;
    cin >> nCases;

    for (int i = 0; i < nCases; i++)
    {
        unsigned long back  = 1;
        unsigned long front = 2;
        unsigned long total = front;

        unsigned long maxNumber;
        std::cin >> maxNumber;

        while (true)
        {
            unsigned long frontPrime = front;

        front = (3 * front) + (2 * back);
        back  = (2 * frontPrime) + back;
        if (front > maxNumber)
        {
            break;
        }
         else
         {
            total += front;
         }
        }

        cout << total << '\n';
    }

    return 0;
}
