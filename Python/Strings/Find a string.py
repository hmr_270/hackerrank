"""
  HARDIK RANA
 
 HACKERRANK DOMAIN:python
 
 """
s1 = input()
s2 = input()

# print s1.count(s2) -- for non-overlapping sequences

# Below is code for overlapping sequences
cnt = 0
for i in range(len(s1)):
    if s1[i:].startswith(s2):
        cnt += 1
print(cnt)
