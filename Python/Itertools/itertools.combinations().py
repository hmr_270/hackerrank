"""
  HARDIK RANA
 
 HACKERRANK DOMAIN:python(itertools)
 
 """

import itertools 
s,n = list(map(str,input().split()))
s = sorted(s)
for i in range(0,int(n)):
    for p in list(itertools.combinations(s,int(i+1))):
        print(''.join(p))
