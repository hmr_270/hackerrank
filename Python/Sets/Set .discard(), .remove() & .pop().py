"""
  HARDIK RANA
 
 HACKERRANK DOMAIN:python
 
 """
n=int(input())
set1=set(map(int,input().split()))
m=int(input())
for i in range(m):
    p=input().split()
    if(p[0]=="remove"):
        set1.remove(int(p[1]))
    elif(p[0]=="discard"):
        set1.discard(int(p[1]))
    else:
        set1.pop()
        
#print(type(set1))
r=list(set1)
q=sum(r)
print(q)
        
        
