"""
  HARDIK RANA
 
 HACKERRANK DOMAIN:python
 
 """
m=int(input())
arr=list(map(int,input().split()))
set1=set(arr)
n=int(input())
arr=list(map(int,input().split()))
set2=set(arr)
symm1=set1.difference(set2)
symm2=set2.difference(set1)
symm=symm1.union(symm2)
r=sorted(symm)
for i in r:
    print(i)
