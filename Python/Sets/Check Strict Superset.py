"""
  HARDIK RANA
 
 HACKERRANK DOMAIN:python
 
 """

set1=set(map(int,input().split()))
t=int(input())
flag=0
for i in range(0,t):
    set2=set(map(int,input().split()))
    if set2.issubset(set1):
        flag=1
    else:
        flag=0
    
if flag==1:
    print("True")
else:
    print("False\n")
