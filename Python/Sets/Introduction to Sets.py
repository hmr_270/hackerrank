"""
  HARDIK RANA
 
 HACKERRANK DOMAIN:python
 
 """
n=int(input())
arr=list(map(int,input().split()))
num_set=set(arr)
sum=0;
count=0;
for i in num_set:
    sum+=i
    count+=1
    
avg=float(sum/count)
print(avg)
    
