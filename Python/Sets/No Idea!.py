"""
  HARDIK RANA
 
 HACKERRANK DOMAIN:python
 
 """
n,m=map(int,input().split())

arr=map(int,input().split()))
assert len(arr) == n

set1=set(map(int,input().split()))
assert len(set1) == m

set2=set(map(int,input().split()))
assert len(set2) == m

result=0

for i in arr:
    if i in set1:
        result+=1
    elif i in set2:
        result-=1

print(result)
