"""
  HARDIK RANA
 
 HACKERRANK DOMAIN:python
 
 """
n=int(input())
set1=set(map(int,input().split()))
m=int(input())
for i in range(0,m):
    p=input().split()
    set2=set(map(int,input().split()))
    if(p[0]=="intersection_update"):
        set1.intersection_update(set2)
    elif(p[0]=="update"):
        set1.update(set2)
    elif(p[0]=="symmetric_difference_update"):
        set1.symmetric_difference_update(set2)
    elif(p[0]=="difference_update"):
        set1.difference_update(set2)
    
print(sum(list(set1)))
        
    
