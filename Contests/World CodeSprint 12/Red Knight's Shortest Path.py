/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(World CodeSprint 12 )
 
 */

#!/bin/python3

 

import sys

 

def printShortestPath(n, i_start, j_start, i_end, j_end):

    if i_start%2==0:

        if i_end%2!=0:

            print("Impossible")

            return

    else:

        if i_end%2==0:

            print("Impossible")

            return

    if j_start%2==0:

        if (abs(i_start-i_end)/2)%2==0:

            if j_end%2!=0:

                print("Impossible")

                return

        else:

            if j_end%2==0:

                print("Impossible")

                return

    else:

        if (abs(i_start-i_end)/2)%2==0:

            if j_end%2==0:

                print("Impossible")

                return

        else:

            if j_end%2!=0:

                print("Impossible")

                return

    s=""

    count=0

    while(1):

        if i_start==i_end and j_start==j_end:

            break

        elif i_start==i_end:

            if j_start<j_end:

                s+='R '

                j_start+=2

                count+=1

            else:

                s+='L '

                j_start-=2

                count+=1

        elif j_start==j_end:

            if i_start<i_end:

                x=(i_end-i_start)//4

                while(x):

                    s+='LR '

                    i_start+=2

                    j_start+=1

                    x-=1

                    count+=1

            else:

                x=(i_start-i_end)//4

                while(x):

                    s+='UL '

                    i_start-=2

                    j_start-=1

                    x-=1

                    count+=1

        elif i_start<i_end and j_start<j_end:

            x=(i_end-i_start)//2

            y=j_end-j_start

            if(y>x):

                s+='R '

                j_start+=2

                count+=1

            else:

                s+='LR '

                i_start+=2

                j_start+=1

                count+=1

        elif i_start>i_end and j_start>j_end:

            s+='UL '

            i_start-=2

            j_start-=1

            count+=1

        elif i_start<i_end and j_start>j_end:

            jumps=(i_end-i_start)//2

            tr=(j_start-j_end)

            jumps=(jumps-tr)//2

            if jumps>=1:

                while(jumps):

                    s+='LR '

                    i_start+=2

                    j_start+=1

                    count+=1

                    jumps-=1

            else:

                s+='LL '

                i_start+=2

                j_start-=1

                count+=1

 

        else:

            s+='UR '

            i_start-=2

            j_start+=1

            count+=1

    print(count)

    print(s)

           

            

 

if __name__ == "__main__":

    n = int(input().strip())

    i_start, j_start, i_end, j_end = input().strip().split(' ')

    i_start, j_start, i_end, j_end = [int(i_start), int(j_start), int(i_end), int(j_end)]

    printShortestPath(n, i_start, j_start, i_end, j_end)
