/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(World CodeSprint 12 )
 
 */

#include<bits/stdc++.h>
using namespace std;
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        int n;
        cin>>n;
        int a[n];
        for(int i=0;i<n;i++)
        {
            cin>>a[i];
        }
        sort(a,a+n);
        int sum=0;
        for(int i=0;i<n-1;i++)
        {
            int r=a[i+1];
            //printf("%d\n",a[i+1]);
            int e=a[i];
            //printf("%d\n",a[i]);
            int u=abs(r-e);
            //printf("%d\n",u);
            sum=sum+u;
        }
        printf("%d\n",sum);
    }
}
