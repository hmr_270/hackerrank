/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(World CodeSprint 12 )
 
 Note:This is not the perfect solution.I got ACC for only 2 test-cases and rest all test-cases i got Time-out.
 */

#include<bits/stdc++.h>
using namespace std;

long long int factorial(int n);
#define max1 1000000000


int main()
{
  int n,m;
  int l,r,d;
  scanf("%d %d",&n,&m);
  long int a[n+1],v;
  for(int i=1;i<=n;i++)
  {
      scanf("%ld",&a[i]);
  }
  while(m--)
  {
    scanf("%d %d %d",&d,&l,&r);
    long long int sum=0;
    if(d==1)
    {
        for(int i=l;i<=r;i++)
        {
            a[i]=a[i]+1;
        }
    }
    else if(d==2)
    {
      for(int i=l;i<=r;i++)
      {
         long long int fact1=factorial(a[i]);
         //printf("%lld ",fact1);
         sum=sum+fact1;
         sum=sum%max1;
         //printf("%lld\n",sum);
      }
      printf("%lld\n",sum);
    }
    else if(d==3)
    {
      a[l]=r;
      /*for(int i=1;i<=n;i++)
      {
          printf("%d ",a[i]);
      }*/
    }
  }
}
long long int factorial(int n) {
   int c;
   long long int result = 1;

   for (c = 1; c <= n; c++)
      result = result*c;

   return result;
}

