"""
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(HackerRank Hiring Contest)
 
 """

#!/bin/python3

import sys

def simplestSum(k, a, b):
    # Complete this function
    mod=1000000007
    sum=0
    x=1
    while(1):
        sum=(sum+((b-a+1)*x)%mod)%mod
        x=k*x+1
        if a<x:
            a=x
        if b<a:
            break
    return sum

if __name__ == "__main__":
    q = int(input().strip())
    for a0 in range(q):
        k, a, b = input().strip().split(' ')
        k, a, b = [int(k), int(a), int(b)]
        result = simplestSum(k, a, b)
        print(result)
