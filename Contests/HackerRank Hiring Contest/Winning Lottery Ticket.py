"""
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(HackerRank Hiring Contest)
 
 """

n=int(input())
store=[0]*1024
for l in range(n):
	a=input()
	count=[0]*10
	for k in range(len(a)):
		count[int(a[k])]=1
	s=0
	for i in range(9,-1,-1):
		if count[i]==1:
			s+=2**i
	store[s]+=1
ans=0
for i in range(1024):
		for j in range(i+1,1024):
			if i | j==1023:
				ans+=store[i]*store[j]
		if i==1023:
			ans+=store[i]*(store[i]-1)//2
print(ans)
