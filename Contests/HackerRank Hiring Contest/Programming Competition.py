"""
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(HackerRank Hiring Contest)
 
 """

n=int(input())
ranklist=[None]*201
count=[0]*201
max=0
for i in range(201):
	ranklist[i]=[None]*1000
for i in range(n):
	string=input().split()
	x=int(string[2])-int(string[1])
	if max<x:
	    max=x
	ranklist[x][count[x]]=string[0]
	count[x]+=1
print(ranklist[max][0],max)
