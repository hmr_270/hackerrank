/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(101 HACK 52)
 
 */

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int k;
    cin >> k;
    cout << 1LL * k * k * k << endl;

    return 0;
}
