/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(HourRank25)
 
 Note:This is not perfect solution.I got only 5 test-cases AC and rest for all test-cases i got TLE.
 
 */

#include <bits/stdc++.h>
using namespace std;

long long int findGCD(int a[], int l,int r);

long long int function1(int l,int r,int a[],int n);

long long int findsum(int a[],int l,int r);

long long int findmax(int a[],int l,int r);

long long int findmax2(long long int answer[],int k);

long long int gcd(long long int a, long long int b);


int main()
{
    int n;
    scanf("%d",&n);
    int a[n+1];
    for(int i=1;i<=n;i++)
    {
        scanf("%d",&a[i]);
    }
    int k=0;
    long long int answer[100000];
    for(int i=1;i<=n;i++)
    {
        for(int j=i;j<=n;j++)
        {
           answer[k]=function1(i,j,a,n);
           k++;
        }
    }
    long long int max2=findmax2(answer,k);
    printf("%lld\n",max2);
}
long long int function1(int l,int r,int a[],int n)
{
    long long int gcd=findGCD(a,l,r);
    long long int sum=findsum(a,l,r);
    long long int max1=findmax(a,l,r);
    long long int result;
    result=gcd*(sum-max1);
    return result;

}
long long int findGCD(int a[], int l,int r)
{
    long long int result = a[l];
    for (int i=l+1; i<=r; i++)
        result = gcd(a[i], result);

    return result;
}
long long int findsum(int a[],int l,int r)
{
   long long int sum=0;
   for(int i=l;i<=r;i++)
   {
       sum+=a[i];
   }
   return sum;
}
long long int findmax(int a[],int l,int r)
{
    long long int max1=a[l];
    for(int i=l+1;i<=r;i++)
    {
        if(a[i]>max1)
            max1=a[i];
    }
    return max1;
}
long long int findmax2(long long int answer[],int k)
{
    long long int max2=answer[0];
    for(int i=1;i<k;i++)
    {
        if(answer[i]>max2)
            max2=answer[i];
    }
    return max2;
}

long long int gcd(long long int a, long long int b)
{
    if (a == 0)
        return b;
    return gcd(b%a, a);
}
