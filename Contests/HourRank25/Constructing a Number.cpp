/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(HourRank25)
 
 */


#include<bits/stdc++.h>
using namespace std;
int main()
{
    int t;
    scanf("%d",&t);
    while(t--)
    {
        int n;
        cin>>n;
        int a[n];
        long long int sum=0;
        for(int i=0;i<n;i++)
        {
            scanf("%d",&a[i]);
        }
        for(int i=0;i<n;i++)
        {
            sum+=a[i];
        }
        if(sum%3==0)
            printf("Yes\n");
        else
            printf("No\n");
    }

}
