/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(RookieRank 4)
 
 */

#include<bits/stdc++.h>
using namespace std;

struct node
{
    int data;
    struct node *left,*right;
};

struct node* insert(struct node *root,long int x);
struct node *newnode(long int data);
long int maxDepth(struct node* node);

//int path[100];

int main()
{
    struct node *root=NULL;
    long int n;
    scanf("%ld",&n);
    while(n--)
    {
        long int k;
        scanf("%ld",&k);
        root=insert(root,k);
    }

     long int w=maxDepth(root);
     cout<<w-1<<endl;
     long int q=pow(2,w-1)-1;
     cout<<q<<endl;
     return 0;
}

struct node* insert(struct node *root,long int x)
{
   if(root==NULL)
     return newnode(x);

   if(x<root->data)
      root->left=insert(root->left,x);

   else if(x>root->data)
      root->right=insert(root->right,x);

   return root;
}

struct node *newnode(long int data)
{
    struct node *ptr=(struct node*)malloc(sizeof(struct node));
    ptr->data=data;
    ptr->left=NULL;
    ptr->right=NULL;
    return ptr;
}

long int maxDepth(struct node* node)
{
   if (node==NULL)
       return 0;
   else
   {
       /* compute the depth of each subtree */
       long int lDepth = maxDepth(node->left);
       long int rDepth = maxDepth(node->right);

       /* use the larger one */
       if (lDepth > rDepth)
           return(lDepth+1);
       else return(rDepth+1);
   }
}
