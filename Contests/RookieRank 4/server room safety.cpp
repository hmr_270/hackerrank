/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(RookieRank 4)
 
 */

#include<bits/stdc++.h>
using namespace std;
int main()
{
    long int n;
    scanf("%ld",&n);
    long int pos[n];
    long int height[n];
    long int right=0;
    long int left=0;
    for(long int i=0;i<n;i++)
    {
        scanf("%ld",&pos[i]);
    }
    for(long int i=0;i<n;i++)
    {
        scanf("%ld",&height[i]);
    }
    for(long int i=0;i<n-1;i++)
    {
        if(pos[i]+height[i]>=pos[i+1])
            left++;
    }
    for(long int i=n-1;i>0;i--)
    {
        if(pos[i]-height[i]<=pos[i-1])
            right++;
    }
    if(left==n-1 && right==n-1)
    {
        printf("BOTH\n");
    }
    else if(left==n-1 && right<n-1)
    {
        printf("LEFT\n");
    }
    else if(right==n-1 && left<n-1)
    {
        printf("RIGHT\n");
    }
    else if(left<n-1 && right<n-1)
    {
        printf("NONE\n");
    }
    return 0;
}
