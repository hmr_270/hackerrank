/*
 HARDIK RANA
 
 HACKERRANK DOMAIN:contests(RookieRank 4)
 
 */


#include<bits/stdc++.h>
using namespace std;
int main()
{
    long int n,tm;
    scanf("%ld %ld",&n,&tm);
    long int count1=0;
    long int a[n];
    for(long int i=0;i<n;i++)
    {
        scanf("%ld",&a[i]);
    }
    sort(a,a+n);
    for(long int i=0;i<n;i++)
    {
        if(a[i]<=tm)
            count1++;
        tm=tm-a[i];
    }
    printf("%ld\n",count1);
}
