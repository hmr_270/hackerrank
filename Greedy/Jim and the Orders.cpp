/* 
   Hardik rana
   
   Hackerrank-Greedy
*/

#include<bits/stdc++.h>
using namespace std;


struct job
{
    long int pid,t,d,r;
};

bool comparison(job s1, job s2)
{
    return (s1.r< s2.r);
}


int main()
{
    long int n;
    scanf("%ld",&n);
    job arr[n];
    //job arr2[n];
    for(long int i=0;i<n;i++)
    {
        arr[i].pid=i+1;
        scanf("%ld %ld",&arr[i].t,&arr[i].d);
    }
    for(long int i=0;i<n;i++)
    {
        arr[i].r=(arr[i].t+arr[i].d);
    }
    sort(arr,arr+n,comparison);

    for(long int i=0;i<n;i++)
    {
        printf("%ld ",arr[i].pid);
    }
    return 0;
}
