/* 
   Hardik rana
   
   Hackerrank Domain-Greedy
*/


#include<bits/stdc++.h>

using namespace std;

int main()
{
    long long int n;
    cin>>n;
    long long int arr[n];
    for(long long int i=0;i<n;i++)
    {
        cin>>arr[i];
    }
    sort(arr,arr+n,greater<long long int>());

    long long int ct=0;
    for(long long int i=0;i<n;i++)
    {
        //printf("%d\n",arr[i]);
        //printf("%d\n",pow(2,i));
        ct+=(arr[i]*pow(2,i));
        //printf("%d\n",ct);
    }
    printf("%lld\n",ct);
}
