/*
  HARDIK RANA
  
  HACKERRANK DOMAIN: C++
  
*/

#include <iostream>
#include <cstdio>
using namespace std;

int main() 
{
    // Complete the code.
    int a;
    long b;
    char ch;
    long long c;
    float d;
    double f;
    scanf("%d %ld %lld %c %f %lf  ", &a,&b,&c,&ch,&d,&f);
    printf("%d\n%ld\n%lld\n%c\n%f\n%lf",a,b,c,ch,d,f);
    return 0;
}
