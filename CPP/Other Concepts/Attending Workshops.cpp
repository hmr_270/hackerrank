/*
  HARDIK RANA
  
  HACKERRANK DOMAIN: C++
  
*/


#include<bits/stdc++.h>
using namespace std;

struct Workshop {
	int start_time, duration, end_time;
};

struct Available_Workshops {
	int n;
	Workshop wks[100000];
};

Available_Workshops * initialize(int start_time[], int duration[], int N) {
	Available_Workshops *av = new Available_Workshops;
	(*av).n = N;
	for (int i = 0; i < N; i++) {
		(av->wks[i]).start_time = start_time[i];
		(av->wks[i]).duration = duration[i];
		(av->wks[i]).end_time = start_time[i] + duration[i];
	}
	return av;
}

bool compare(const pair<int, int>& a, const pair<int, int>& b) {
	return a.second < b.second;
}

int CalculateMaxWorkshops(Available_Workshops *ptr) {
	int count = 1, min;
	int j = (*ptr).n;
	vector<pair<int, int>> a;
	for (int i = 0; i < j; i++) {
		a.push_back(pair<int, int>((ptr->wks[i]).start_time, (ptr->wks[i]).end_time));
	}
	sort(a.begin(), a.end(), compare);
	min = a[0].second;
	for (int i = 1; i < j; i++) {
		if (a[i].first >= min) {
			count++;
			min = a[i].second;
		}
	}
	return count;
}



int main(int argc, char *argv[]) {
    int n; // number of workshops
    cin >> n;
    // create arrays of unknown size n
    int* start_time = new int[n];
    int* duration = new int[n];

    for(int i=0; i < n; i++){
        cin >> start_time[i];
    }
    for(int i = 0; i < n; i++){
        cin >> duration[i];
    }

    Available_Workshops * ptr;
    ptr = initialize(start_time,duration, n);
    cout << CalculateMaxWorkshops(ptr) << endl;
    return 0;
}
