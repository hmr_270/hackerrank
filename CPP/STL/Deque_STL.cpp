/*
  HARDIK RANA
  
  HACKERRANK DOMAIN: C++
  
*/

#include <iostream>
#include <deque>
using namespace std;

int main()
{
    int N;
    cin >> N;
    while(N--)
    {
        int n,k,com;
        cin >> n >> k;
        deque < int > mydeq;
        deque < int > :: iterator it;
        pair < int , deque <int> :: iterator > place;
        place.first = 0;
        com = n - k ;
        while ( n-- )
        {
            int t;
            cin >> t;
            mydeq.push_back (t);
        }
        int t = com+1;
        while ( t-- )
        {
            if( place.first !=0 )
                place.second--;
            if(place.first == 0)
            {
                for( it = mydeq.begin() ; it != mydeq.begin()+k ; it++ )
                    if( *it > place.first )
                    {
                        place.first = *it;
                        place.second = it;
                    }
            }
            else
            {
                it = mydeq.begin()+k-1;
                if( *it >= place.first )
                {
                    place.first = *it;
                    place.second = it;
                }
            }
            if(t == com)
                cout << place.first;
            else
                cout << ' ' << place.first;
            if ( place.second -mydeq.begin() <= 0  )
                place.first = 0;
            mydeq.erase( mydeq.begin() );
        }
        cout<<endl;
    }
    return 0;
}

